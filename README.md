# IDS 721 Individual Project 2
[![pipeline status](https://gitlab.com/hxia5/ids-721-week-10/badges/main/pipeline.svg)](https://gitlab.com/hxia5/ids-721-week-10/-/commits/main)

## Overview
* This is my repository of IDS 721 week 10 mini project - CRust Serverless Transformer Endpoint.

## Purpose
- Dockerize Hugging Face Rust transformer
- Deploy container to AWS Lambda
- Implement query endpoint

## Query Endpoint

Default query "I stayed up all night to": https://pbbe3vtrit7xqw5utx3qxzmvr40fvzjn.lambda-url.us-east-1.on.aws/

![alt text](<default deployment.png>)

Custom query: https://pbbe3vtrit7xqw5utx3qxzmvr40fvzjn.lambda-url.us-east-1.on.aws/?query="Your query here"

![alt text](custom.png)

## Docker Image in ECR Container Registry 

![alt text](docker.png)

## Local Test on 9000

![alt text](local.png)

## Lambda Function in AWS

![alt text](lambda.png)

## Key Steps

1. Install Rust and Cargo-Lambda, instructions can be found [here](https://www.cargo-lambda.info/guide/installation.html) and [here](https://www.rust-lang.org/tools/install).

2. Create a new Rust project using the following command:
```bash
cargo lambda new ids-721-week-10
```

3. Choose a hugging face model that is smaller enough to be ran on lambda, here is one: https://huggingface.co/rustformers, also, download the  `.bin` file and run `brew install git-lfs`

4. Add this in Cargo.toml dependencies:
```toml
[dependencies]
llm = { git = "https://github.com/rustformers/llm" , branch = "main" }
```

5. Midify the `main.rs`, then run `cargo lambda watch` to test on localhost 9000.

6. At AWS IAM, create a new user with `lambdafullaccess`, `iamfullaccess` and `AmazonEC2ContainerRegistryFullAccess` policies.

7. Store your `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION` in `.env` file and Gitlab.

8. At AWS ECR, create a new private repository, then click the repository, click `View push commands`, copy them into `.gitlab-ci.yml` and create variables `EC` and `ECR` in `.env` and Gitlab.

9. Create a `Dockerfile` and a new `.gitlab-ci.yml` file that can deploy the docker image to ECR.

10. Push the code to Gitlab, and the pipeline will run automatically. After it's passed, the docker image will be pushed to ECR.

11. Create lambda function in AWS using the docker image, choose `arm64` as the architecture.

12. Click `Configuration`, then `General Configuration`, then edit to change memory to the max limit and change timeout to the max minutes (hint will be shown).

16. Go back to `Configuration`, then click `Fucntion URL` to create a new function URL, choose NONE, BUFFERED and enable CORS

17. Test your fucntion URL







## References
1. https://git-lfs.com/
2. https://huggingface.co/rustformers
3. https://huggingface.co/rustformers/pythia-ggml/tree/main
4. https://github.com/rustformers/llm?tab=readme-ov-file
